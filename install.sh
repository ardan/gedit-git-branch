#!/bin/sh
#
# Installs the plugin to the users folder

PLUGIN_FOLDER=~/.local/share/gedit/plugins/

# Install plugin
mkdir -pv $PLUGIN_FOLDER
cp -v gedit-git-branch.plugin $PLUGIN_FOLDER
cp -Rv gedit-git-branch $PLUGIN_FOLDER

echo "gedit-git-branch installed!"
