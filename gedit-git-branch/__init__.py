# Show the active git branch in the status bar when a file is loaded

from gi.repository import GObject, Gtk, Gedit
import os
import re
import pipes
import commands

BRANCH_REGEX = re.compile(r"^\* (\w+)")

class GeditGitBranchGlobal:
    window = 0

class GeditGitBranchWindow(GObject.Object, Gedit.WindowActivatable):

    __gtype_name__ = "GeditGitBranchWindow"
    window = GObject.property(type=Gedit.Window)

    def __init__(self):
      GObject.Object.__init__(self)

    def do_activate(self):
      self.statusbar = self.window.get_statusbar()
      self.doc = self.window.get_active_document()
      GeditGitBranchGlobal.window = self

    def do_deactivate(self):
      pass

class GeditGitBranch(GObject.Object, Gedit.ViewActivatable):
    __gtype_name__ = "GeditGitBranch"
    view = GObject.property(type=Gedit.View)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self.doc = self.view.get_buffer()
        self.handler_id = self.doc.connect("loaded", self.on_document_loaded)
        self.context_id = GeditGitBranchGlobal.window.statusbar.get_context_id('Git_Branch')
        self.statusbar = GeditGitBranchGlobal.window.statusbar;

    def do_deactivate(self):
        self.doc.disconnect(self.handler_id)
        self.statusbar.remove_all(self.context_id)

    def do_update_state(self):
        pass

    def on_document_loaded(self, *action):
        label = None
        path = self.doc.get_location().get_path()
        base_dir = os.path.dirname(path)
        out = commands.getoutput('cd "' + base_dir + '" && git branch')

        for el in out.split('\n'):
            if  el[0] == '*':
                match = BRANCH_REGEX.search(el)
                label = match.group(1)

        if label is not None:
            self.addMsgToStatusbar('Git branch: ' + label)
        else:
            self.addMsgToStatusbar('No git repository in current directory.')

    def addMsgToStatusbar(self, msg):
        self.message_id = self.statusbar.push(self.context_id, msg.strip())

# EOF
