# Gedit Git Branch

Displays the current git branch in the status bar upon opening a document.

## Installation

Clone repo. Navigate into gedit-git-branch. Run ./install.sh

Open gedit then enable Git Branch plugin.

## Usage

Open any document. The current git branch will be displayed in the status bar.
